#
#===============================================================================
#
#         FILE:  Define.pm
#
#  DESCRIPTION:  
#
#        FILES:  ---
#         BUGS:  ---
#        NOTES:  ---
#       AUTHOR:   (), <>
#      COMPANY:  
#      VERSION:  1.0
#      CREATED:  04/14/2010 11:50:17 PM GMT
#     REVISION:  ---
#===============================================================================

use strict;
use warnings;

package GoogPl::Define;

use Net::Dict;

sub register {
    my $self = shift;

    return ('cmd' => 'define', 'pat' => '^!define\s+(.*)');
}

sub help {
    my $self = shift;
    return ('Define terms via dict lookup');
}

sub process_command {
    my $self = shift;
    my ($kernel, $sender, $nickhost, $target, $msgtxt, %conf) = @_;

    $msgtxt =~ /^!define\s+(.*)/;

    my $dict = Net::Dict->new($conf{'dict'});
    my $r = $dict->define($1, $conf{'dict_db'});
    if( ! scalar @$r ) {
	$kernel->post($sender => privmsg => $target => "Definition not found.");
	return;
    }
    my $def = $r->[0][1];
    $def =~ s/[\r\n]/ /g;
    $def =~ s/ \s+/ /g;
    $kernel->post($sender => privmsg => $target => $def);
}

1;
