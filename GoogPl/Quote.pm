#
#===============================================================================
#
#         FILE:  Quote.pm
#
#  DESCRIPTION:  
#
#        FILES:  ---
#         BUGS:  ---
#        NOTES:  ---
#       AUTHOR:   (), <>
#      COMPANY:  
#      VERSION:  1.0
#      CREATED:  04/14/2010 11:50:17 PM GMT
#     REVISION:  ---
#===============================================================================

use strict;
use warnings;

package GoogPl::Quote;

use LWP::UserAgent;
use Text::CSV;
use JSON qw(decode_json);

sub register {
    my $self = shift;

    return ('cmd' => 'quote', 'pat' => '^!quote\s+([^ ]+)');
}

sub help {
    my $self = shift;
    return (
	    'Look up stock quotes via Alpha Vantage',
	   );
}

sub process_command {
    my $self = shift;
    my ($kernel, $sender, $nickhost, $target, $msgtxt, %conf) = @_;

    $msgtxt =~ /^!quote\s+([^ ]+)/;

    my $stock = uc($1);
    
    my $ua = LWP::UserAgent->new;
# f defined by http://www.gummy-stuff.org/Yahoo-data.htm
    my $url = "https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol=$stock&apikey=$conf{'alphavantage_api_key'}";

    my $response = $ua->get($url);

    if (!$response->is_success) {
        return;
    }
    my $result = decode_json($response->decoded_content);
    my $quote = $result->{'Global Quote'};

    $kernel->post($sender => privmsg => $target =>
	    sprintf('%s price: %s (%s/%s) close: %s open: %s high: %s low: %s',
            $quote->{'01. symbol'},
            $quote->{'05. price'},
            $quote->{'09. change'},
            $quote->{'10. change percent'},
            $quote->{'08. previous close'},
            $quote->{'02. open'},
            $quote->{'03. high'},
            $quote->{'04. low'}
		));
}

1;
