#
#===============================================================================
#
#         FILE:  Countdown.pm
#
#  DESCRIPTION:  
#
#        FILES:  ---
#         BUGS:  ---
#        NOTES:  ---
#       AUTHOR:   (), <>
#      COMPANY:  
#      VERSION:  1.0
#      CREATED:  04/20/2010 06:06:11 PM GMT
#     REVISION:  ---
#===============================================================================

use strict;
use warnings;

package GoogPl::Countdown;

use Convert::Age;
use Time::ParseDate;

sub register {
    my $self = shift;

    return ('cmd' => 'countdown', 'pat' => '^!countdown\s+(.+)');
}

sub help {
    my $self = shift;
    
    return ('Displays the time until the date/time specified');
}

sub process_command {
    my $self = shift;
    my ($kernel, $sender, $nickhost, $target, $msgtxt, %conf) = @_;

    $msgtxt =~ /^!countdown\s+(.+)/;
    my $time = $1;

    my $t = parsedate(uc($time), 'UK' => $conf{'uk_dates'});

    if( ! defined $t ) {
	return;
    }

    if( $t - time < 0 ) {
	$kernel->post($sender => privmsg => $target =>
			sprintf('%s occured: %s ago',
				$time, Convert::Age::encode(time - $t)));
    }
    else {
	$kernel->post($sender => privmsg => $target =>
	    		sprintf('Time until %s: %s',
	    			$time, Convert::Age::encode($t - time)));
    }
}

1;
