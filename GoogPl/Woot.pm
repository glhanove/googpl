#
#===============================================================================
#
#         FILE:  Woot.pm
#
#  DESCRIPTION:  
#
#        FILES:  ---
#         BUGS:  ---
#        NOTES:  ---
#       AUTHOR:   (), <>
#      COMPANY:  
#      VERSION:  1.0
#      CREATED:  04/14/2010 11:50:17 PM GMT
#     REVISION:  ---
#===============================================================================

use strict;
use warnings;

package GoogPl::Woot;

use LWP::UserAgent;
use XML::RSS;

my %wootoff;

sub register {
    my $self = shift;

    return ('cmd' => 'woot', 'pat' => '^!woot(off)?\s*(.*)');
}

sub help {
    my $self = shift;

    return ('List current woot sales. No argument to get woot.com, otherwise specify sellout, kids, home, wine, or shirt.',
	    'If a wootoff is underway, you can use !wootoff to start auto-notification of new items.',
	    'Auto timer is disabled when wootoff ends.');
}

sub process_command {
    my $self = shift;
    my ($kernel, $sender, $nickhost, $target, $msgtxt, %conf) = @_;

    if( $msgtxt =~ /^!wootoff\s*(.*)/ ) {
	my $type = $1 ? $1 : 'woot';
	if( $wootoff{$target.':'.$type} ) {
	    delete $wootoff{$target.':'.$type};
	}
	else {
	    $wootoff{$target.':'.$type} = {'argv'=> [ @_ ], 'lastwoot'=>''};
	}
	$kernel->delay('GoogPl::Woot->wootoff' => 1);
	return;
    }

    get_woot($kernel, $sender, $nickhost, $target, $msgtxt, undef, %conf);
}

sub wootoff {
    my ($self, $kernel) = @_;
    if( ! scalar keys %wootoff ) {
	return;
    }
    while( my ($k, $v) = each %wootoff ) {
	my ($lkernel, $sender, $nickhost, $target, $msgtxt, %conf) = @{$v->{'argv'}};
	get_woot($lkernel, $sender, $nickhost, $target, $msgtxt, $k, %conf);
    }
    $kernel->delay('GoogPl::Woot->wootoff' => 60);
}

sub get_woot {
    my ($kernel, $sender, $nickhost, $target, $msgtxt, $key, %conf) = @_;
    
    $msgtxt =~ /^!woot(?:off)?\s*(.*)/;
    
    my $url;
    if( $1 =~ /sellout/ ) {
	$url = 'http://sellout.woot.com/salerss.aspx';
    }
    elsif( $1 =~ /kids/ ) {
	$url = 'http://kids.woot.com/salerss.aspx';
    }
    elsif( $1 =~ /home/ ) {
	$url = 'http://home.woot.com/salerss.aspx';
    }
    elsif( $1 =~ /wine/ ) {
	$url = 'http://wine.woot.com/salerss.aspx';
    }
    elsif( $1 =~ /tech/ ) {
	$url = 'http://tech.woot.com/salerss.aspx';
    }
    elsif( $1 =~ /sport/ ) {
	$url = 'http://sport.woot.com/salerss.aspx';
    }
    elsif( $1 =~ /shirt/ ) {
	$url = 'http://shirt.woot.com/salerss.aspx';
    }
    elsif( $1 =~ /deals/ ) {
	$url = 'http://deals.woot.com/deals.rss?page=1';
    }
    else {
	$url = 'http://www.woot.com/salerss.aspx';
    }
    my $ua = LWP::UserAgent->new;
    my $response = $ua->get($url);
    if( ! $response->is_success ) {
        my $html = $response->decoded_content;
        $html =~ s/[\r\n]/  /g;
        $kernel->post($sender => privmsg => $target =>
		'LWP failed');
	return;
    }

    my $rss = XML::RSS->new;
    if( ! $rss->parse($response->decoded_content) ) {
	$kernel->post($sender => privmsg => $target =>
		'XML failed to parse');
	return;
    }
    if( ! $rss->{'items'} ) { return; }

    my $item = $rss->{'items'}[0]{'http://www.woot.com/'};

    my $msg = "";
    if( lc($item->{'wootoff'}) eq 'true' ) {
	if( defined $key ) {
	    if( $rss->{'items'}[0]{'title'} eq $wootoff{$key}{'lastwoot'} ) { return; }
	    $wootoff{$key}{'lastwoot'} = $rss->{'items'}[0]{'title'};
	}
	$msg .= '*WOOTOFF*';
    }
    elsif( defined $key ) {
	delete $wootoff{$key};
    }

    if( lc($item->{'soldout'}) eq 'true' ) {
	$msg .= '*SOLDOUT*';
    }

    if( $msg ne '' ) { $msg .= ' '; }
    if( $item->{'blogurl'} eq '') {
	$item->{'blogurl'} = $url;
	$item->{'blogurl'} =~ s/salerss.aspx$//;
    }
    $kernel->post($sender => privmsg => $target =>
		    sprintf('%s%s / %s / %s', $msg, $rss->{'items'}[0]{'title'},
			    $item->{'price'}, $item->{'blogurl'}));
}

1;
