#
#===============================================================================
#
#         FILE:  Search.pm
#
#  DESCRIPTION:  
#
#        FILES:  ---
#         BUGS:  ---
#        NOTES:  ---
#       AUTHOR:   (), <>
#      COMPANY:  
#      VERSION:  1.0
#      CREATED:  04/14/2010 11:32:02 PM GMT
#     REVISION:  ---
#===============================================================================

use strict;
use warnings;

package GoogPl::Search;

use HTML::Entities;
use GoogPl::GooGl;

use LWP::UserAgent;
use JSON qw(decode_json);

sub register {
    my $self = shift;

    return ('cmd' => 'search', 'pat' => '^!(search|image)\s+(.*)');
}

sub help {
    my $self = shift;

    return (
            'Perform various search functions via Google. Available search commands are: ',
            '!search Google WWW search',
            '!image Google Image search',
           );
}

sub process_command {
    my $self = shift;
    my ($kernel, $sender, $nickhost, $target, $msgtxt, %conf) = @_;

    my $ua = LWP::UserAgent->new;
    my $url = 'https://www.googleapis.com/customsearch/v1?'
        . 'safe=off&cx=006592992791136391943%3Aybhpu8uoyj0'
        . '&key='.$conf{'googl_api_key'};

    if( $msgtxt =~ /^!search\s+(.*)/ ) {
        $url .= '&q=' . $1;
        
        my $response = $ua->get($url);
        if (!$response->is_success) { return; }
        my $res = decode_json($response->decoded_content);

        my $r = $res->{'items'}[0];

        $kernel->post($sender => privmsg => $target => 
                sprintf('%s (%s)', $r->{'title'}, $r->{'link'}));
        return;
    }
    elsif( $msgtxt =~ /^!image\s+(.*)/ ) {
        $url .= '&searchType=image&q='.$1;
        my $response = $ua->get($url);
        if (!$response->is_success) { return; }
        my $res = decode_json($response->decoded_content);

        my $r = $res->{'items'}[0];
        my $responseMsg = $r->{'title'};
        if (length($r->{'link'}) > $conf{'min_length'}) {
            my $shortUrl = GoogPl::GooGl::get_short_url($r->{'link'}, %conf);
            $responseMsg .= sprintf(' (%s / %s)', $shortUrl, $r->{'link'});
        } else {
            $responseMsg .= sprintf(' (%s)', $r->{'link'});
        }
        $kernel->post($sender => privmsg => $target => $responseMsg);
        return;
    }
}

1;
