#
#===============================================================================
#
#         FILE:  Rot13.pm
#
#  DESCRIPTION:  
#
#        FILES:  ---
#         BUGS:  ---
#        NOTES:  ---
#       AUTHOR:   (), <>
#      COMPANY:  
#      VERSION:  1.0
#      CREATED:  04/20/2010 06:06:11 PM GMT
#     REVISION:  ---
#===============================================================================

use strict;
use warnings;

package GoogPl::Rot13;

sub register {
    my $self = shift;

    return ('cmd' => 'rot13', 'pat' => '^!rot13\s+(.*)');
}

sub help {
    my $self = shift;
    
    return ('rot13 encode/decode message');
}

sub process_command {
    my $self = shift;
    my ($kernel, $sender, $nickhost, $target, $msgtxt, %conf) = @_;

    $msgtxt =~ /^!rot13\s+(.*)/;
    my $m = $1;

    $m =~ tr/a-zA-Z/n-za-mN-ZA-M/;
    $kernel->post($sender => privmsg => $target => "rot13: $m");
}

1;
