#
#===============================================================================
#
#         FILE:  Poniez.pm
#
#  DESCRIPTION:  !ud deppy
#
#        FILES:  ---
#         BUGS:  ---
#        NOTES:  ---
#       AUTHOR:   (), <>
#      COMPANY:  
#      VERSION:  1.0
#      CREATED:  04/20/2010 06:06:11 PM GMT
#     REVISION:  ---
#===============================================================================

use strict;
use warnings;

package GoogPl::Poniez;

my @poniez = (
"-18             ,%,13_-1",
"-18            %%%13/,\\-1",
"-113         _.-\"%%13|//8%-1",
"-113      _.' _.-\"  /8%%%-1",
"-113  _.-'_.-\"12 O1)    13\\8%%%-1",
"-113 /.\.'            \\8%%%-1",
"-113 \\ /        _,     |8%%%-1",
"-113  `\"-----\"~`\\   _,*'\\8%%'   13_,--\"\"\"\"-,8%%,-1",
"-113             )*^     `\"\"~~`          \\8%%%,-1",
"-113           _/                         \\8%%%-1",
"-113       _.-`/                           |8%%,13___-1",
"-113   _.-\"   /      ,           ,        ,|8%%13   .`\\-1",
"-113  /\\     /      /             `\\       \\8%'13   \\ /-1",
"-113  \\ \\ _,/      /`~-._         _,`\\      \\8`13\"\"~~`-1",
"-113   `\"` /-.,_ /'      `~\"----\"~    `\\     \\-1",
"-113       \\___,'                       \\.-\"`/-1",
"-113                                     `--'-1",
);

my @hug = (
" 1,1 .11_____1.1......11_____1................9_____________1...-1",
" 11,1 [12hhhhh11]1.... 11[12hhhhh11]1..............9/3GGGGGGGGGGGGG9}1..-1",
" 11,1 [12hhhhh11]1.... 11[12hhhhh11]13___1......13____9{3GGGGGGGGGGGGGG9}1..-1",
" 11,1 [12hhhhh11]1.... 11[12hhhhh11]6uuu13|1....13|6uuuu13|3GGG9}------{3GGG9}1..-1",
" 11,1 [12hhhhh11]_____11[12hhhhh11]6uuu13|1....13|6uuuu13|3GGG9}1...... 9----1..-1",
" 11,1 [12hhhhhhhßhhhhhhhhh11]6uuu13|1....13|6uuuu13|3GGG9}1.............-1",
" 11,1 [12hhhhhhhhhhhhhhhhh11]6uuu13|1....13|6uuuu13|3GGG9}1....9,,,,,,,1..-1",
" 11,1 [12hhhhh11]-----[12hhhhh11]6uuu13|1....13|6uuuu13|3GGG9}1....9{3GGGGGG9}1.-1",
" 11,1 [12hhhhh11]1.... 11[12hhhhh11]6uuu13|1....13|6uuuu13|3GGG9}_______|3GGG9}1.-1",
" 11,1 [12hhhhh11]1.... 11[12hhhhh11]6uuu13\\,,,,/6uuuu13|3GGGGGGGGGGGGGGG9}1.-1",
" 11,1 [12hhhhh11]1....11 [12hhhhh11]13\\6uuuuuuuuuuuu13/9'\\3GGGGGGGGGGGGG9/1.-1",
);

# coming soon, pagoda

my %lastcall = ();

sub register {
    my $self = shift;

    return ('cmd' => 'poniez', 'pat' => '^!poniez\s*(.*)');
}

sub help {
    my $self = shift;
    
    return ('poniez');
}

sub process_command {
    my $self = shift;
    my ($kernel, $sender, $nickhost, $target, $msgtxt, %conf) = @_;

    if( time - (defined($conf{'poniez_limit'})?$conf{'poniez_limit'}:43200) < $lastcall{$target}||0 ) { return; }

    $msgtxt =~ /^!poniez\s*(.*)/;
    my @ascii = @poniez;
    if( lc($1) eq 'hug' ) {
	@ascii = @hug;
    }

    for( my $i = 0; $i < scalar(@ascii); $i++ ) {
	$kernel->post($sender => privmsg => $target => $ascii[$i]);	
    }
    $lastcall{$target} = time;
}

1;
