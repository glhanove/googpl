#
#===============================================================================
#
#         FILE:  IMDB.pm
#
#  DESCRIPTION:  
#
#        FILES:  ---
#         BUGS:  ---
#        NOTES:  ---
#       AUTHOR:   (), <>
#      COMPANY:  
#      VERSION:  1.0
#      CREATED:  04/14/2010 11:50:17 PM GMT
#     REVISION:  ---
#===============================================================================

use strict;
use warnings;

package GoogPl::IMDB;

use LWP::UserAgent;
use JSON qw(decode_json);

sub register {
    my $self = shift;

    return ('cmd' => 'imdb', 'pat' => '^!imdb\s+(.+)');
}

sub help {
    my $self = shift;

    return ('Search IMDB for movie titles.');
}

sub process_command {
    my $self = shift;
    my ($kernel, $sender, $nickhost, $target, $msgtxt, %conf) = @_;

    $msgtxt =~ /^!imdb\s+(.+)/;
    my $search = $1;
    $search =~ s/ /+/g;
    my $url = "http://www.imdbapi.com/?t=$search";
    my $ua = LWP::UserAgent->new;
    my $response = $ua->get($url);
    if( ! $response->is_success ) {
	return;
    }
    my $res = decode_json($response->decoded_content);

    open LOG, '>/home/ghanover/imdb.log';
    use Data::Dumper;
    print LOG Data::Dumper::Dumper($res);
    close LOG;

    if( ! $res->{'Title'} ) {
        $kernel->post($sender => privmsg => $target => "Not Found");
	return;
    }

    $kernel->post($sender => privmsg => $target =>
		    sprintf('%s :: %s (%s/10) %s http://www.imdb.com/title/%s/', 
			    $res->{'Title'}, $res->{'Year'}, $res->{'Rating'},
			    $res->{'Plot'}, $res->{'ID'}
			    )
		    );
}

1;
