#
#===============================================================================
#
#         FILE:  TLD.pm
#
#  DESCRIPTION:  
#
#        FILES:  ---
#         BUGS:  ---
#        NOTES:  ---
#       AUTHOR:   (), <>
#      COMPANY:  
#      VERSION:  1.0
#      CREATED:  04/14/2010 11:50:17 PM GMT
#     REVISION:  ---
#===============================================================================

use strict;
use warnings;

package GoogPl::TLD;

use Net::Domain::TLD;

my %tld = (
    %{Net::Domain::TLD::tlds('cc')},
    %{Net::Domain::TLD::tlds('gtld_open')},
    %{Net::Domain::TLD::tlds('gtld_restricted')},
    %{Net::Domain::TLD::tlds('new_open')},
    %{Net::Domain::TLD::tlds('new_restricted')},
    );

sub register {
    my $self = shift;

    return ('cmd' => 'tld', 'pat' => '^!tld\s+\.?([a-z]+)');
}

sub help {
    my $self = shift;

    return ('Top Level Domain lookup');
}

sub process_command {
    my $self = shift;
    my ($kernel, $sender, $nickhost, $target, $msgtxt, %conf) = @_;

    $msgtxt =~ /^!tld\s+\.?([a-z]+)/;

    my $t = lc($1);
    if( grep /$t/, keys %tld ) {
	$kernel->post($sender => privmsg => $target => $tld{$t});
	return;
    }
    $kernel->post($sender => privmsg => $target => "TLD not found")
}

1;
