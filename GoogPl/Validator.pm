#
#===============================================================================
#
#         FILE:  Validator.pm
#
#  DESCRIPTION:  Parsing using XML::Simple because SOAP::Parser was throwing
#  errors on the envelope
#
#        FILES:  ---
#         BUGS:  ---
#        NOTES:  ---
#       AUTHOR:   (), <>
#      COMPANY:  
#      VERSION:  1.0
#      CREATED:  04/20/2010 06:06:11 PM GMT
#     REVISION:  ---
#===============================================================================

use strict;
use warnings;

package GoogPl::Validator;

use XML::Simple;
use LWP::UserAgent;
use URI::Escape qw( uri_escape );

sub register {
    my $self = shift;

    return ('cmd' => 'validate', 'pat' => '^!validate\s+(http:\/\/.*)');
}

sub help {
    my $self = shift;
    
    return ('Validate a URI at validator.w3.org');
}

sub process_command {
    my $self = shift;
    my ($kernel, $sender, $nickhost, $target, $msgtxt, %conf) = @_;

    $msgtxt =~ /^!validate\s+(http:\/\/.*)/;
    my $url = $1;

    my $ua = LWP::UserAgent->new;
    $ua->timeout(15);
    my $resp = $ua->get('http://validator.w3.org/check?output=soap12&uri='.uri_escape($url));

    if( ! $resp->is_success ) {
	return;
    }

    my $xs = XML::Simple->new;
    my $xml = $xs->XMLin($resp->decoded_content);
    my $v = $xml->{'env:Body'}{'m:markupvalidationresponse'};

    my $msg;
    if( $v->{'m:validity'} eq 'true' ) {
	$msg = 'Page is valid '.$v->{'m:doctype'};
    }
    else {
	$msg = 'Page is not valid '.$v->{'m:doctype'};
    }

    $msg .= ' errorcount:'.$v->{'m:errors'}{'m:errorcount'}.' warningcount:'.$v->{'m:warnings'}{'m:warningcount'};

    $kernel->post($sender => privmsg => $target => $msg);
}

1;
