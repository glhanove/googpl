#
#===============================================================================
#
#         FILE:  BOFH.pm
#
#  DESCRIPTION:  
#
#        FILES:  ---
#         BUGS:  ---
#        NOTES:  ---
#       AUTHOR:   (), <>
#      COMPANY:  
#      VERSION:  1.0
#      CREATED:  04/14/2010 11:50:17 PM GMT
#     REVISION:  ---
#===============================================================================

use strict;
use warnings;

package GoogPl::BOFH;

use LWP::UserAgent;

sub register {
    my $self = shift;

    return ('cmd'=>'bofh', 'pat' => '^!bofh\s*(.*)');
}

sub help {
    my $self = shift;
    return (
	    'Get BOFH excuse calendar results. No argument will return todays excuse',
	    'Use rand to get a random excuse, or specify a date to get the excuse for a particular date'
	   );
}

sub process_command {
    my $self = shift;
    my ($kernel, $sender, $nickhost, $target, $msgtxt, %conf) = @_;

    $msgtxt =~ /^!bofh\s*(.*)/;

    my $ua = LWP::UserAgent->new();
    my $arg = $1;
    my $add_url = '';
    if( $arg =~ /rand/ ) {
# do nothing
    }
    elsif( $arg ) {
# assume date
	$add_url = "date=$arg";
    }
    else {
	$add_url = "eod";
    }
    my $res = $ua->get("http://www.bofhcalendar.com/api.php?$add_url");
    $kernel->post($sender => privmsg => $target => $res->decoded_content);
}

1;
