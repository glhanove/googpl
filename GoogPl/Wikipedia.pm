#
#===============================================================================
#
#         FILE:  Wikipedia.pm
#
#  DESCRIPTION:  
#
#        FILES:  ---
#         BUGS:  ---
#        NOTES:  ---
#       AUTHOR:   (), <>
#      COMPANY:  
#      VERSION:  1.0
#      CREATED:  04/20/2010 06:06:11 PM GMT
#     REVISION:  ---
#===============================================================================

use strict;
use warnings;

package GoogPl::Wikipedia;

use WWW::Wikipedia;
use HTML::Entities;

sub register {
    my $self = shift;

    return ('cmd' => 'wiki', 'pat' => '^!wiki\s+(.*)');
}

sub help {
    my $self = shift;
    
    return ('Perform wikipedia search and return first result');
}

sub process_command {
    my $self = shift;
    my ($kernel, $sender, $nickhost, $target, $msgtxt, %conf) = @_;

    my $wiki = WWW::Wikipedia->new;

    $msgtxt =~ /^!wiki\s+(.*)/;

    my $result = $wiki->search($1);

    if( ! $result ) {
	$kernel->post($sender => privmsg => $target => "No result found");
	return;
    }

    if( $result->text ) {
	my $text = $result->text_basic;
# strip out meta data
	$text =~ s/\{\{[^}]*?\}\}//mg;
# strip out references
	$text =~ s/<ref[^>]*\/>/ /mg;
	$text =~ s/<ref[^>]*>.*?<\/ref>/ /mg;
# strip out extra endlines and spaces
	$text =~ s/[\r\n ]+/ /mg;
	$kernel->post($sender => privmsg => $target => decode_entities($text));
    }
    else {
	$kernel->post($sender => privmsg => $target => "No results");
    }
}

1;
