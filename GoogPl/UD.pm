#
#===============================================================================
#
#         FILE:  UD.pm
#
#  DESCRIPTION:  
#
#        FILES:  ---
#         BUGS:  ---
#        NOTES:  ---
#       AUTHOR:   (), <>
#      COMPANY:  
#      VERSION:  1.0
#      CREATED:  04/14/2010 11:46:09 PM GMT
#     REVISION:  ---
#===============================================================================

use strict;
use warnings;

package GoogPl::UD;

use LWP::UserAgent;
use HTML::Entities;

sub register {
    my $self = shift;

    return ('cmd' => 'ud', 'pat' => '^!ud\s+(.*)');
}

sub help {
    my $self = shift;

    return ('Lookup term via urbandictionary.com');
}

sub process_command {
    my $self = shift;
    my ($kernel, $sender, $nickhost, $target, $msgtxt, %conf) = @_;

    $msgtxt =~ /!ud\s+(.*)/;

    my $ua = LWP::UserAgent->new;
    my $res = $ua->get("http://www.urbandictionary.com/define.php?term=$1");

    if( $res->decoded_content =~ m/<div id='not_defined_yet'>/m ) {
        $kernel->post($sender => privmsg => $target => "This term is not defined yet");
        return;
    }
    if( $res->decoded_content =~ m/<div class=["']meaning["']>(.*?)<\/div>/sm ) {
        my $def = $1;
        print "$def\n";
        $def =~ s/<.*?>//g;
        $def =~ s/[\r\n]//g;
        my $ex = '';
        if( $res->decoded_content =~ m/<div class="example">(.*?)<\/div>/m ) {
            $ex = $1;
            $ex =~ s/<.*?>//g;
            $ex =~ s/[\r\n]//g;
            $ex = " Example: $ex";
        }
        $kernel->post($sender => privmsg => $target => decode_entities($def.$ex));
    }
}

1;
