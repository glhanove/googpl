#
#===============================================================================
#
#         FILE:  Weather.pm
#
#  DESCRIPTION:  
#
#        FILES:  ---
#         BUGS:  ---
#        NOTES:  ---
#       AUTHOR:   (), <>
#      COMPANY:  
#      VERSION:  1.0
#      CREATED:  04/14/2010 10:57:08 PM GMT
#     REVISION:  ---
#===============================================================================

use strict;
use warnings;

package GoogPl::Weather;

use LWP::UserAgent;
use JSON qw(decode_json);
use URI::Escape qw(uri_escape);
use Data::Uniqid qw(uniqid);
use Digest::SHA qw(hmac_sha1_base64);
use URI::Query;

sub register {
    my $self = shift;

    return ('cmd' => 'weather', 'pat' => '^!?weather\s+(.*)');
}

sub help {
    my $self = shift;
    return ('Get current weather information via Google weather. For best results, search on city, state or zipcode');
}

sub buildBaseString {
    my $baseURI = shift;
    my $method = shift;
    my $params = shift;

    my @r;

    foreach my $key (sort {lc $a cmp $b} keys %$params) {
        push @r, "$key=".uri_escape($params->{$key});
    }

    return $method .'&'. uri_escape($baseURI) .'&'. uri_escape(join('&', @r));
}

sub buildAuthorizationHeader {
    my $oauth = shift;
    my $signature = shift;
    my $r = 'OAuth ';
    my @values;
    foreach my $key (sort keys %$oauth) {
        push @values, "$key=\"".uri_escape($oauth->{$key})."\"";
    }

    $r .= join(', ', @values);
    $r .= ', oauth_signature="'.uri_escape($signature).'"';

    return $r;
}

sub process_command {
    my $self = shift;
    my ($kernel, $sender, $nickhost, $target, $msgtxt, %conf) = @_;

    $msgtxt =~ /^weather\s+(.*)/;
    my $loc = $1;

    my $url = 'https://weather-ydn-yql.media.yahoo.com/forecastrss';
    my $app_id = $conf{'weather_app_id'};
    my $consumer_key = $conf{'weather_consumer_key'};
    my $consumer_secret = $conf{'weather_consumer_secret'};

    my $query = {
        'format'   => 'json',
        'location' => $loc,
    };

    my $oauth = {
        'oauth_consumer_key'     => $consumer_key,
        'oauth_nonce'            => uniqid(),
        'oauth_signature_method' => 'HMAC-SHA1',
        'oauth_timestamp'        => time(),
        'oauth_version'          => '1.0'
    };
    my $base_info = buildBaseString($url, 'GET', {%$query, %$oauth});

    my $composite_key = uri_escape($consumer_secret) . '&';

    my $oauth_signature = hmac_sha1_base64($base_info, $composite_key);
    while (length($oauth_signature) % 4) {
        $oauth_signature .= '=';
    }

    my $headers = {
        'Authorization'  => buildAuthorizationHeader($oauth, $oauth_signature),
        'X-Yahoo-App-Id' => $app_id
    };
    my $ua = LWP::UserAgent->new;
    my $qq = URI::Query->new($query);

    my $r = $ua->get($url .'?'. $qq->stringify, %$headers);

    my $wjson = decode_json($r->decoded_content);

    if (!defined($wjson->{'current_observation'})) {
        $kernel->post($sender => privmsg => $target => "weather forecast not found");
        return;
    }

    $kernel->post($sender => privmsg => $target =>
        sprintf('Current for %s: %s %sF(%dC), Humidity %s%%, Wind %s, High of %sF, Low of %sF',
            $wjson->{'location'}{'city'},
            $wjson->{'current_observation'}{'condition'}{'text'},
            $wjson->{'current_observation'}{'condition'}{'temperature'},
            ($wjson->{'current_observation'}{'condition'}{'temperature'} - 32) * 5/ 9,
            $wjson->{'current_observation'}{'atmosphere'}{'humidity'},
            $wjson->{'current_observation'}{'wind'}{'speed'},
            $wjson->{'forecasts'}[0]{'high'},
            $wjson->{'forecasts'}[0]{'low'}
        )
    );
    return;
}

1;
