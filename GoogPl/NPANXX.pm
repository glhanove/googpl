#
#===============================================================================
#
#         FILE:  NPANXX.pm
#
#  DESCRIPTION:  
#
#        FILES:  ---
#         BUGS:  ---
#        NOTES:  ---
#       AUTHOR:   (), <>
#      COMPANY:  
#      VERSION:  1.0
#      CREATED:  04/20/2010 06:06:11 PM GMT
#     REVISION:  ---
#===============================================================================

use strict;
use warnings;

package GoogPl::NPANXX;

use LWP::UserAgent;

sub register {
    my $self = shift;

    return ('cmd' => 'npanxx', 'pat' => '^!npanxx\s+([\d]+)(?:\s+)?([\d]+)?');
}

sub help {
    my $self = shift;
    
    return ('Search for Area Code / Exchange location');
}

sub process_command {
    my $self = shift;
    my ($kernel, $sender, $nickhost, $target, $msgtxt, %conf) = @_;

    my $ua = LWP::UserAgent->new;

    $msgtxt =~ /^!npanxx\s+([\d]+)(?:\s+)?([\d]+)?/;
    my ($npa, $nxx) = ($1, $2);

    my $url = "http://puck.nether.net/npa-nxx/lookup.cgi?npa=$npa&nxx=$nxx";

    my $res = $ua->get($url);
    return if ! $res->is_success;

    my %result;
    if( $res->decoded_content =~ m/<br>State = (.*?)<br>/ ) {
	$result{'state'} = $1;
    }
    my @items = ( 'npa', 'nxx', 'clli', 'city', 'telco' );
    foreach my $i (@items) {
	if( $res->decoded_content =~ m/test2.cgi\?$i=([^"]*)/m ) {
	    $result{$i} = $1;
	}
    }

    if( $result{'city'} ) {
	$kernel->post($sender => privmsg => $target =>
		sprintf('%s, %s; clli:%s; telco:%s', 
		    $result{'city'}, $result{'state'}, $result{'clli'},
		    $result{'telco'}));
    }
    elsif( $result{'state'} ) {
	$kernel->post($sender => privmsg => $target => $result{'state'});
    }
}

1;
